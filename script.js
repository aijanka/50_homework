
class Machine{
    constructor(){
        this.isOn = false;
    }

    turnOn(){
        this.isOn = true;
        console.log("Machine is turned on now)");
    };
    turnOff(){
        this.isOn = false;
        console.log("Machine is turned off now)");
    };
}

class HomeAppliance extends Machine{
    constructor(){
        super();
        this.isPluggedIn = false;
    }

    plugIn(){
        this.isPluggedIn = true;
        console.log("Home Appliance is plugged in now)");
    };
    plugOff(){
        this.isPluggedIn = false;
        console.log("Home Appliance is plugged out now)");
    };
}

class WashingMachine extends HomeAppliance{
    constructor(){
        super();
    }
    run(){
        if(this.isPluggedIn){
            this.turnOn();
        } else {
            console.log("This machine is not plugged in!");
        }
    };
}

class LightSource extends HomeAppliance{
    constructor(){
        super();
        this.level = 0;
    }

    setLevel(level){
        if(level > 0 && level <= 100 && this.isPluggedIn){
            this.level = level;
        }
    };
}

class AutoVehicle extends Machine{
    constructor(){
        super();
        this.x = 0;
        this.y = 0;
    }
    setPosition(x, y){
        this.x = x;
        this.y = y;
    };
}
class Car extends AutoVehicle{
    constructor(){
        super();
        this.speed = 10;
    }

    setSpeed(speed){this.speed = speed;};
    run(x, y){
        console.log(`The car departs from (${this.x}, ${this.y}) to (${x},${y}) with the speed:  ${this.speed}`);

        this.directionX = (x - this.x) < 0 ? -1 : 1;
        this.directionY = (y - this.y) < 0 ? -1 : 1;
        let limitUpValue = (current, end, v) => {
            return (end - current) * v >= 0 ? current : end;
        };

        let changePosition = () => {
            let newX = this.x + this.speed * this.directionX;
            let newY = this.y + this.speed * this.directionY;
            this.setPosition(limitUpValue(newX, x, this.directionX), limitUpValue(newY, y, this.directionY));

            console.log(`The car arrived to: (${this.x},${this.y})`);

        };

        if (this.isOn) {
            let runTime = setInterval(() => {
                changePosition();
                if (this.x === x && this.y === y) {
                    console.log("Your car arrived");
                    clearTimeout(runTime);
                }
            }, 1000);
        }
    };
};



const bosch = new WashingMachine();
bosch.plugIn();
bosch.turnOn();

const lightBulb = new LightSource();
lightBulb.plugIn();
lightBulb.setLevel(60);
lightBulb.turnOn();

const honda = new Car();
honda.setPosition(30, 40);
honda.turnOn();
honda.setSpeed(60);
honda.run(180, 240);